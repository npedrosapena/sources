/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ficheiros;

import java.io.File;

/**
 *
 * @author nelson
 */
public class Ficheiros 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        //creamos un obxeto tipo File
        /*
            test.txt -> archivo con números
            testCadenas.txt -> archivo con cadenas
        */
        
        File lectorFicheiro = new File("test.txt");//le pasamos la ruta y nombre de archivo
        File novoLector = new File("/dos/linuxMint/Programacion/Java/ProyectosNetbeans/ficheiros/test2.txt");
        LerFich.lerPalabras(lectorFicheiro);
       // LerFich.leerNumeros(novoLector);
        LerFich.lerSeparadores(novoLector);
        
        File lectorObxetos = new File("persoas.txt");
        LerFich.lerObxetos(lectorObxetos);
    }
    
}
