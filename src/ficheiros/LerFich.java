
package ficheiros;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author nelson
 */
public class LerFich
{
   public static void lerPalabras(File datos)
   {
       Scanner sc = null;
      try{ 
           //abrimos el flujo de datos
           sc= new Scanner(datos);
       
           //mientras no llega al final del archivo
           while(sc.hasNext())
           {
               //leemos los datos
               String info = sc.next();
               System.out.println(info);
           } 
      }catch(Exception ex)
      {
          JOptionPane.showMessageDialog(null, "Error: ",ex.getMessage(),JOptionPane.ERROR_MESSAGE);
      }finally
      {
          if(sc!=null)
          {
           //al salir, cerramos el flujo de datos con el archivo
           sc.close();
          }
      }
       
   } 
   
   public static void leerNumeros(File datos)
   {
        Scanner sc = null;
      try{ 
           //abrimos el flujo de datos
           sc= new Scanner(datos);
       
           //mientras no llega al final del archivo
           while(sc.hasNext())
           {
               //leemos los datos
               Integer info = sc.nextInt();
               System.out.println(info);
           } 
      }catch(Exception ex)
      {
          JOptionPane.showMessageDialog(null,"Error: "+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
      }finally
      {
          if(sc!=null)
          {
           //al salir, cerramos el flujo de datos con el archivo
           sc.close();
          }
      }
   }
   
   public static void lerSeparadores(File datos)
   {
       Scanner sc=null;
       try
       {
           sc=new Scanner(datos).useDelimiter(",");//le indicamos el delimitador
           
           while (sc.hasNext())
           {
               String info=sc.next();
               System.out.println(info);
           }
       } catch (FileNotFoundException ex)
       {
           JOptionPane.showMessageDialog(null,"Error: "+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
       }finally
       {
           if (sc!=null)
           {
               sc.close();
           }
       }
       
   }
   
   public static void lerObxetos(File datos)
   {
       Scanner sc= null;
       ArrayList<Persoa> lista= new ArrayList<>();
       
       try
       {
           sc = new Scanner(datos);
           
           while(sc.hasNext())
           {
               String cadena=sc.nextLine();
               String arrayDatos[]=cadena.split(",");
               Persoa persona=new Persoa(arrayDatos[0],arrayDatos[1],Integer.parseInt(arrayDatos[2]));
               lista.add(persona);
           }
       } catch (FileNotFoundException ex)
       {
           JOptionPane.showMessageDialog(null,"Error: "+ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
       }finally
       {
           if(sc!=null)
           {
               sc.close();
           }
       }
       
       for(int i=0;i<lista.size();i++)
       {
           System.out.println(lista.get(i));
       }
       
       
   }
}
