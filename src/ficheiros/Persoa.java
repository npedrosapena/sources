/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ficheiros;

/**
 *
 * @author nelson
 */
public class Persoa
{
    private String nome;
    private String dni;
    private int idade;
    
    public Persoa(){}
    public Persoa(String nome, String dni, int idade)
    {
        this.nome=nome;
        this.dni=dni;
        this.idade=idade;
    }

    /**
     * @return the nome
     */
    public String getNome()
    {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome)
    {
        this.nome = nome;
    }

    /**
     * @return the dni
     */
    public String getDni()
    {
        return dni;
    }

    /**
     * @return the idade
     */
    public int getIdade()
    {
        return idade;
    }

    @Override
    public String toString()
    {
        return "Persoa{" + "nome=" + nome + ", dni=" + dni + "," + idade + '}';
    }
    
  
}
